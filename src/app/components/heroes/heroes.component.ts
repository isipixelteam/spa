import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from './../../services/heroes.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {

  heroes:Heroe[] = [];

  constructor( private _heroesService:HeroesService, private router: Router, private activatedRoute:ActivatedRoute ) {
    
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe( ( params ) => {
      if( params.hasOwnProperty('termino') ){
        console.log( 'prop' );
        this.heroes = this._heroesService.buscarHeroes( params['termino'] );
        this.heroes
      } else {
        this.heroes = this._heroesService.getHeroes();
        console.log( 'hola' );
      }
    });

  }

  verHeroe( nombrex: string ){
    this.router.navigate( ['/heroe', nombrex] );
  }

  buscarHeroe( termino:string ){
    this.router.navigate( ['/heroes', termino] );
  }
}
